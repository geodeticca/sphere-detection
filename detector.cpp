//
// Created by andrijdavid on 5/12/21.
//
#include <iostream>
#include <tuple>
#include "library.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/filesystem/path.hpp>
#include <thread>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/integral_image_normal.h>


namespace fs = boost::filesystem;
using namespace std::chrono_literals;

template<typename PointType>
pcl::visualization::PCLVisualizer::Ptr
simpleVis(typename pcl::PointCloud<PointType>::ConstPtr cloud) {
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(0, 0, 0.5);
    viewer->addPointCloud<PointType>(cloud, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    //viewer->addCoordinateSystem (1.0, "global");
    viewer->initCameraParameters();
    return (viewer);
}

template<typename PointType>
void find_sphere(const std::string &plyfile, const fs::path &outFil) {
    typename pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>);
    pcl::io::loadPLYFile(plyfile, *cloud);

    showStats<PointType>(*cloud);

    // The point clouds
    typename pcl::PointCloud<PointType>::Ptr passthrough_filtered(new pcl::PointCloud<PointType>);
    typename pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>);
    typename pcl::PointCloud<PointType>::Ptr remove_plane_cloud(new pcl::PointCloud<PointType>);
    typename pcl::PointCloud<PointType>::Ptr sphere_cloud(new pcl::PointCloud<PointType>);
    typename pcl::PointCloud<PointType>::Ptr sphere_output(new pcl::PointCloud<PointType>);
    typename pcl::PointCloud<PointType>::Ptr sphere_RANSAC_output(new pcl::PointCloud<PointType>);
    typename pcl::PointCloud<PointType>::Ptr sphere_output_total(new pcl::PointCloud<PointType>);

    std::vector<int> inliers;

    cloud_filtered = voxel_filter<PointType>(cloud);
    passthrough_filtered = passthrough_filter<PointType>(cloud_filtered);
    remove_plane_cloud = remove_plane<PointType>(passthrough_filtered);

    passthrough_filtered.swap(remove_plane_cloud);
    std::cout << "Passthrough filter without plane " << passthrough_filtered->points.size() << " point" << std::endl;

    std::vector<pcl::PointIndices> cluster_indices = cluster_cloud<PointType>(passthrough_filtered, 0.20);
    std::cout << "Found " << cluster_indices.size() << " clusters" << std::endl;

    std::vector<pcl::PointIndices::Ptr> pointIndices;
    std::vector<typename pcl::PointCloud<PointType>::Ptr> spheres;

    int i = 0;
    for (const auto &cluster_indice : cluster_indices) {

        typename pcl::PointCloud<PointType>::Ptr cluster(new pcl::PointCloud<PointType>);
        pcl::ExtractIndices<PointType> eifilter(
                true); // Initializing with true will allow us to extract the removed indices
        pcl::PointIndices::Ptr indicesPtr(new pcl::PointIndices());
        indicesPtr->indices = cluster_indice.indices;

        eifilter.setInputCloud(passthrough_filtered);
        eifilter.setIndices(indicesPtr);
        eifilter.filter(*cluster); // Our current cluster
//        pcl::PointCloud<pcl::Normal>::Ptr normal = estimate_normals3d<PointType>(cluster);
        SegmentationResult result = find_sphere<PointType>(cluster, 0.01, 0.01, 0.06, 0.17);
        std::cerr << "Sphere coefficients: " << *result.coef << std::endl;

        if (result.is_empty()) {
            std::cerr << "[[--Can't find the Sphere Candidate.--]]" << std::endl;
            continue;
        }
        std::cout << "Found something " << std::endl;
        sphere_output = extract_segmentation<PointType>(cluster, result.ind, true);
        // created RandomSampleConsensus object and compute the appropriated model
        typename pcl::SampleConsensusModelSphere<PointType>::Ptr model_s(
                new pcl::SampleConsensusModelSphere<PointType>(sphere_output));

        pcl::RandomSampleConsensus<PointType> ransac(model_s);
        ransac.setDistanceThreshold(.01);
        ransac.computeModel();
        ransac.getInliers(inliers);

        pcl::copyPointCloud<PointType>(*sphere_output, inliers, *sphere_RANSAC_output);
        double w = 0;
        w = double(sphere_RANSAC_output->width * sphere_RANSAC_output->height)
            / double(sphere_output->width * sphere_output->height);
        if (w > 0.8) {
            std::cout << "found a ball" << std::endl;
            pointIndices.push_back(indicesPtr);
            typename pcl::PointCloud<PointType>::Ptr copy (new pcl::PointCloud<PointType>);
            pcl::copyPointCloud(*sphere_RANSAC_output, *copy);
            spheres.push_back(copy);
            i++;
        } else {
            std::cout << "can not find a ball " << w << " " << std::endl;
        }
    }
    std::cout << "Found " << i << " balls" << std::endl;
    int32_t rgb = (static_cast<uint32_t>(0) << 16 |
                   static_cast<uint32_t>(0) << 8 | static_cast<uint32_t>(128));
    for (auto &p: cloud_filtered->points) p.rgb = rgb;
//    pcl::visualization::PCLVisualizer::Ptr viewer = simpleVis<PointType>(sphere_output_total);
    pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(1, 1, 1);
    for (const auto &sphere : spheres) {
//        pcl::copyPointCloud<PointType>(*passthrough_filtered, *cluster_indice, *sphere_output_total);
        int32_t rgb = (static_cast<uint32_t>(255) << 16 |
                       static_cast<uint32_t>(255) << 8 | static_cast<uint32_t>(0));
        for (auto &p: sphere->points) p.rgb = rgb;
        std::string id = "sample sphere " + std::to_string(rand());
        viewer->addPointCloud<PointType>(sphere, id );
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, id);
    }
    viewer->addPointCloud<PointType>(cloud_filtered, "sample cloud");
//    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->addCoordinateSystem (1.0, "global");
    viewer->initCameraParameters();
    while (!viewer->wasStopped()) {
        viewer->spinOnce(100);
        std::this_thread::sleep_for(100ms);
    }
}


int main(int argc, char **argv) {

    if (argc != 3) {
        std::cout << "Provide input file as an only argument";
        return 1;
    }

    std::string plyfile = argv[1];
    std::cout << "Ply filename" << plyfile << std::endl;
    fs::path outFile = fs::path(argv[2]);
    find_sphere<pcl::PointXYZRGB>(plyfile, outFile);
}