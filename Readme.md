# Sphere Detection

This is a header only library. 
It exposes diverse functions for sphere detection using PCL. 
To use it, you just need to include the header in your source file. 