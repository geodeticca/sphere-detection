#ifndef SPHERE_DETECTION_LIBRARY_H
#define SPHERE_DETECTION_LIBRARY_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>

#include <pcl/search/kdtree.h>

#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>

#include <pcl/sample_consensus/sac_model_sphere.h>
#include <pcl/sample_consensus/ransac.h>

struct SegmentationResult {
    pcl::ModelCoefficients::Ptr coef;
    pcl::PointIndices::Ptr ind;
    pcl::PointIndices::Ptr clusterInd;

    bool is_empty() {
        return ind == nullptr || ind->indices.size() == 0;
    }

    int size() {
        return ind != nullptr ? ind->indices.size() : 0;
    }
};

template<typename PointType>
typename pcl::PointCloud<PointType>::Ptr
voxel_filter(const typename pcl::PointCloud<PointType>::Ptr &cloud, const float size = 0.02f) {
    pcl::VoxelGrid<PointType> voxel_grid;
    typename pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>);
    voxel_grid.setInputCloud(cloud);
    voxel_grid.setLeafSize(size, size, size);
    voxel_grid.filter(*cloud_filtered);
    std::cout << "Voxel filter: " << cloud_filtered->points.size() << " points" << std::endl;
    return cloud_filtered;
}

template<typename PointType>
typename pcl::PointCloud<PointType>::Ptr
passthrough_filter(const typename pcl::PointCloud<PointType>::Ptr &cloud) {
    pcl::PassThrough<PointType> pass;
    typename pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>);
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0, 2.5);
    pass.filter(*cloud_filtered);
    std::cout << "Passthrough filter: " << cloud_filtered->points.size() << " points" << std::endl;
    return cloud_filtered;
}

template<typename PointType>
pcl::PointCloud<pcl::Normal>::Ptr estimate_normals(const typename pcl::PointCloud<PointType>::Ptr &cloud) {
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    pcl::IntegralImageNormalEstimation<PointType, pcl::Normal> ne;
    ne.setNormalEstimationMethod(ne.AVERAGE_3D_GRADIENT);
    ne.setMaxDepthChangeFactor(0.02f);
    ne.setNormalSmoothingSize(10.0f);
    ne.setInputCloud(cloud);
    ne.compute(*normals);
    return normals;
}

template<typename PointType>
pcl::PointCloud<pcl::Normal>::Ptr estimate_normals3d(const typename pcl::PointCloud<PointType>::Ptr &cloud) {
    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<PointType, pcl::Normal> ne;
    ne.setInputCloud(cloud);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    typename pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>());
    ne.setSearchMethod(tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch(0.03);

    // Compute the features
    ne.compute(*cloud_normals);

    return cloud_normals;
}

template<typename PointType>
SegmentationResult detect_plane(const typename pcl::PointCloud<PointType>::Ptr &cloud) {
    pcl::PointIndices::Ptr inliers_plane(new pcl::PointIndices());
    pcl::ModelCoefficients::Ptr coefficients_plane(new pcl::ModelCoefficients());
    pcl::SACSegmentation<PointType> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setAxis(Eigen::Vector3f(0, -1, 0));       // best plane should be perpendicular to z-axis
    seg.setMaxIterations(40);
    seg.setDistanceThreshold(0.05);
    seg.setInputCloud(cloud);
    seg.segment(*inliers_plane, *coefficients_plane);
    SegmentationResult res;
    res.coef = coefficients_plane;
    res.ind = inliers_plane;
    return res;
}

template<typename PointType>
SegmentationResult
detect_plane(const typename pcl::PointCloud<PointType>::Ptr &cloud, const pcl::PointCloud<pcl::Normal>::Ptr &normal) {
    pcl::PointIndices::Ptr inliers_plane(new pcl::PointIndices());
    pcl::ModelCoefficients::Ptr coefficients_plane(new pcl::ModelCoefficients());
    pcl::SACSegmentationFromNormals<PointType, pcl::Normal> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_NORMAL_PARALLEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setAxis(Eigen::Vector3f(0, -1, 0));       // best plane should be perpendicular to z-axis
    seg.setMaxIterations(40);
    seg.setDistanceThreshold(0.05);
    seg.setInputCloud(cloud);
    seg.setInputNormals(normal);
    seg.segment(*inliers_plane, *coefficients_plane);

    SegmentationResult res;
    res.coef = coefficients_plane;
    res.ind = inliers_plane;
    return res;
}

template<typename PointType>
typename pcl::PointCloud<PointType>::Ptr
extract_segmentation(const typename pcl::PointCloud<PointType>::Ptr &cloud, pcl::PointIndices::Ptr ind,
                     const bool inlier = true) {
    pcl::ExtractIndices<PointType> extract_indices;
    typename pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>);
    extract_indices.setInputCloud(cloud);
    extract_indices.setIndices(ind);
    extract_indices.setNegative(!inlier);
    extract_indices.filter(*cloud_filtered);
    return cloud_filtered;
}

template<typename PointType>
typename pcl::PointCloud<PointType>::Ptr remove_plane(typename pcl::PointCloud<PointType>::Ptr &cloud) {
    typename pcl::PointCloud<PointType>::Ptr cloud_f(new pcl::PointCloud<PointType>);
    int nr_points = (int) cloud->size();
    // While 30% of the original cloud is still there
    while (cloud->size() > 0.3 * nr_points) {
        SegmentationResult res = detect_plane<PointType>(cloud);
        if (res.is_empty()) {
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        cloud_f = extract_segmentation<PointType>(cloud, res.ind, false);
        cloud.swap(cloud_f);
    }
    return cloud;
}

template<typename PointType>
typename pcl::PointCloud<PointType>::Ptr
remove_plane_with_normal(typename pcl::PointCloud<PointType>::Ptr &cloud) {
    typename pcl::PointCloud<PointType>::Ptr cloud_f(new pcl::PointCloud<PointType>);
    int nr_points = (int) cloud->size();
    // While 30% of the original cloud is still there
    while (cloud->size() > 0.3 * nr_points) {
        pcl::PointCloud<pcl::Normal>::Ptr normal = estimate_normals3d<PointType>(cloud);
        SegmentationResult res = detect_plane<PointType>(cloud, normal);
        if (res.is_empty()) {
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        cloud_f = extract_segmentation<PointType>(cloud, res.ind, false);
        cloud.swap(cloud_f);
    }
    return cloud;
}


template<typename PointType>
typename std::vector<pcl::PointIndices>
cluster_cloud(const typename pcl::PointCloud<PointType>::Ptr &cloud, const double size = 0.2) {
    std::vector<pcl::PointIndices> cluster_indices;
    typename pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
    tree->setInputCloud(cloud);

    pcl::EuclideanClusterExtraction<PointType> ec;
    ec.setClusterTolerance(size);
    ec.setMinClusterSize(100);
    ec.setMaxClusterSize(25000);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud);
    ec.extract(cluster_indices);
    return cluster_indices;
}

template<typename PointType>
void color_points(typename pcl::PointCloud<PointType>::Ptr cloud, const pcl::PointIndices &indices, int colorIndex) {
    std::vector<uint32_t> colors{
            0x000000, 0xFFFF00, 0x1CE6FF, 0xFF34FF, 0xFF4A46, 0x008941, 0x006FA6, 0xA30059,
            0xFFDBE5, 0x7A4900, 0x0000A6, 0x63FFAC, 0xB79762, 0x004D43, 0x8FB0FF, 0x997D87,
            0x5A0007, 0x809693, 0xFEFFE6, 0x1B4400, 0x4FC601, 0x3B5DFF, 0x4A3B53, 0xFF2F80,
            0x61615A, 0xBA0900, 0x6B7900, 0x00C2A0, 0xFFAA92, 0xFF90C9, 0xB903AA, 0xD16100,
            0xDDEFFF, 0x000035, 0x7B4F4B, 0xA1C299, 0x300018, 0x0AA6D8, 0x013349, 0x00846F,
            0x372101, 0xFFB500, 0xC2FFED, 0xA079BF, 0xCC0744, 0xC0B9B2, 0xC2FF99, 0x001E09,
            0x00489C, 0x6F0062, 0x0CBD66, 0xEEC3FF, 0x456D75, 0xB77B68, 0x7A87A1, 0x788D66,
            0x885578, 0xFAD09F, 0xFF8A9A, 0xD157A0, 0xBEC459, 0x456648, 0x0086ED, 0x886F4C
    };
    for (auto pit = indices.indices.begin(); pit != indices.indices.end(); ++pit) {
        uint32_t c = colors[colorIndex % (colors.size())];
        cloud->points[*pit].r = c >> 16 & 0x0000ff;
        cloud->points[*pit].g = c >> 8 & 0x0000ff;
        cloud->points[*pit].b = c & 0x0000ff;
    }
}

void extract_indices(std::vector<int> &selected, std::vector<int> &orig, std::vector<int> &result);

template<typename PointType>
void showStats(pcl::PointCloud<PointType> &cloud) {
    int minx = -1;
    int miny = -1;
    int minz = -1;
    for (int i = 0; i < cloud.points.size(); i++) {
        PointType &p = cloud.points[i];
        if (minx == -1 || minx > p.x) {
            minx = p.x;
        }
        if (miny == -1 || miny > p.y) {
            miny = p.y;
        }
        if (minz == -1 || minz > p.z) {
            minz = p.z;
        }
    }

    int maxx = -1;
    int maxy = -1;
    int maxz = -1;
    for (int i = 0; i < cloud.points.size(); i++) {
        PointType &p = cloud.points[i];
        if (maxx == -1 || maxx < p.x) {
            maxx = p.x;
        }
        if (maxy == -1 || maxy < p.y) {
            maxy = p.y;
        }
        if (maxz == -1 || maxz < p.z) {
            maxz = p.z;
        }
    }
    int dx = maxx - minx;
    int dy = maxy - miny;
    int dz = maxz - minz;

    std::cout << "dx: " << dx << std::endl
              << "dy: " << dy << std::endl
              << "dz: " << dz << std::endl;

    int v = dx * dy * dz;
    double precision = std::pow((double) v / cloud.points.size(), 1 / 3.);

    std::cout << "Precission: " << (double) v / (double) cloud.points.size() << std::endl;

    std::cout << "Precission: " << precision << std::endl;
    std::cout << "Points: " << cloud.points.size() << std::endl;

}

template<typename PointType>
void detect_plane(const typename pcl::PointCloud<PointType>::Ptr &cloud, pcl::PointIndices &indices,
                  pcl::ModelCoefficients::Ptr &coefficients) {

    pcl::SACSegmentation<PointType> segmentation;

    segmentation.setInputCloud(cloud);
//    segmentation.setInputNormals(cloud);
    segmentation.setModelType(pcl::SACMODEL_PLANE);
    segmentation.setMethodType(pcl::SAC_RANSAC);
    segmentation.setDistanceThreshold(1.5);
//    segmentation.setNormalDistanceWeight(0.1);
    segmentation.setOptimizeCoefficients(true);
    //  segmentation.setRadiusLimits(18,22);
    segmentation.setEpsAngle(1 / (180 / 3.141592654));
    segmentation.setMaxIterations(10000);

    segmentation.segment(indices, *coefficients);
}

template<typename PointType>
SegmentationResult
find_sphere(typename pcl::PointCloud<PointType>::Ptr &cloud, double distance_threshold = 1.25,
            double distance_weight = 0.1, double min_radius = 20, double max_radius = 21,
            int max_iterations = 1000000) {

    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::SACSegmentation<PointType> segmentation;

    segmentation.setInputCloud(cloud);
    segmentation.setModelType(pcl::SACMODEL_SPHERE);
    segmentation.setMethodType(pcl::SAC_RANSAC);
    segmentation.setDistanceThreshold(distance_threshold);
    segmentation.setOptimizeCoefficients(true);
    segmentation.setRadiusLimits(min_radius, max_radius);
    segmentation.setMaxIterations(max_iterations);

    pcl::PointIndices::Ptr indx(new pcl::PointIndices());
    segmentation.segment(*indx, *coefficients);

    if (indx->indices.empty())
        std::cout << ". RANSAC nothing found" << "\n";
    else {
        std::cout << ". RANSAC found shape with [%d] points:" << indx->indices.size()
                  << "\n";
        double part = (double) indx->indices.size() / (double) cloud->points.size() * 100;
        std::cout << "Model coefficient: " << *coefficients << std::endl << "Points: "
                  << indx->indices.size() << " (" << part << "%)" << std::endl;
    }

    SegmentationResult result;
    result.coef = coefficients;
    result.ind = indx;
    return result;
}

template<typename PointType>
SegmentationResult
find_sphere(typename pcl::PointCloud<PointType>::Ptr &cloud, const pcl::PointCloud<pcl::Normal>::Ptr &normal,
            double distance_threshold = 1.25,
            double distance_weight = 0.1, double min_radius = 20, double max_radius = 21,
            int max_iterations = 1000000) {

    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::SACSegmentationFromNormals<PointType, pcl::Normal> segmentation;

    segmentation.setInputCloud(cloud);
    segmentation.setInputNormals(normal);

    segmentation.setModelType(pcl::SACMODEL_NORMAL_SPHERE);
    segmentation.setMethodType(pcl::SAC_RANSAC);
    segmentation.setDistanceThreshold(distance_threshold);
    segmentation.setOptimizeCoefficients(true);
    if (min_radius > 0 || max_radius > 0)
        segmentation.setRadiusLimits(min_radius, max_radius);
    segmentation.setMaxIterations(max_iterations);

    pcl::PointIndices::Ptr indx(new pcl::PointIndices());
    segmentation.segment(*indx, *coefficients);

    if (indx->indices.empty())
        std::cout << ". RANSAC nothing found" << "\n";
    else {
        std::cout << ". RANSAC found shape with [%d] points:" << indx->indices.size()
                  << "\n";
        double part = (double) indx->indices.size() / (double) cloud->points.size() * 100;
        std::cout << "Model coefficient: " << *coefficients << std::endl << "Points: "
                  << indx->indices.size() << " (" << part << "%)" << std::endl;
    }

    SegmentationResult result;
    result.coef = coefficients;
    result.ind = indx;
    return result;
}

#endif //SPHERE_DETECTION_LIBRARY_H
